# ! CAREFUL ! Unusable for now, since the only website used to populate this DataBase is down. Multiple scrappers will be done for multiples websites...

## Purpose of the back end :

This "back end" is designed to be a strong no-sql data base hosted with FireBase of Movies/Tv shows from the "zone-téléchargement" website (for now),
which then, will be used with custom Android Apps and Kodi plugins to create a full Direct Download & user friendly experience. 

### The infrastructure at the beginning of the project

Multiple different infrastructures will be managed later, but the first one which will be used (and so apps designed for) is : 
  * Synology NAS (using DownloadStation package to handle downloads AND premium hosts)
  * Rpi(s)/Odroid using Kodi (plugin using the backend/database update)
  * Android app (using the backend)
  
### Exemple of the data base : 

Here is an exemple of the data base : 

```json
{
    "19912": {
        "average_note": "5.44",
        "id": "19912",
        "poster_path": "/xjlItGz2ltp6VwHUWC1EsVld3S4.jpg",
        "qualities": {
            "1080p | multilangues | truefrench": {
                "1fichier": {
                    "not_premium": ["http://www.dl-protect.com/74383696"],
                    "premium": ["http://www.dl-protect.com/CD56A5E9"]
                },
                "known_hosts": true,
                "rapidgator": {
                    "not_premium": ["http://www.dl-protect.com/16CA9AAA"]
                },
                "turbobit": {
                    "not_premium": ["http://www.dl-protect.com/E5DAF95D"],
                    "premium": ["http://www.dl-protect.com/9DF464D0"]
                },
                "uplea": {
                    "not_premium": ["http://www.dl-protect.com/37550472"],
                    "premium": ["http://www.dl-protect.com/83123921"]
                },
                "uploaded": {
                    "not_premium": ["http://www.dl-protect.com/0FA25F63"],
                    "premium": ["http://www.dl-protect.com/F4CD603C"]
                },
                "uptobox": {
                    "not_premium": ["http://www.dl-protect.com/7A11250C"],
                    "premium": ["http://www.dl-protect.com/7DE499AB"]
                },
                "url": "https://www.zone-telechargement.com/dvd-bluray/1080p/150-telecharger-destination-finale-4-megaupload-1080p-french.html"
            }
        },
        "release_date": "2009-08-26",
        "title": "Destination Finale 4",
        "upload_date": "2010-09-02"
    }
}
```
        
The db is Json formated (easilly to use than other ones).
Each Movie/Tv Show will be identified by its TMDB (The Movie DataBase) id, and several information about it will be then retrieved in the db.


#### Project and Readme still in progress...