package com.zonedldb.DataBase;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.zonedldb.Movie.Host;

import java.util.concurrent.CountDownLatch;

/**
 * Created by ghyslain on 22/07/2016.
 */
public final class FirebaseCalls {

    private static DataSnapshot dataInMovieDataBase;
    private static DataSnapshot dataInQualityDataBase;
    private static final Firebase rootRef = new Firebase("https://zondedldb.firebaseio.com");

    private static void addUrl(String currentMovieId, String currentMovieQuality, String currentMovieUrl, CountDownLatch done) {

        rootRef.child("movies").child(currentMovieId).child("qualities").child(currentMovieQuality).child("url").setValue(currentMovieUrl, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                if (firebaseError != null) {
                    System.out.println("url could not be saved " + firebaseError.getMessage());
                    done.countDown();
                } else {
                    done.countDown();
                }
            }
        });
    }

    private static void addingId(String currentMovieId, CountDownLatch done) {

        rootRef.child("movies").child(currentMovieId).child("id").setValue(currentMovieId, (firebaseError, firebase) -> {
            if (firebaseError != null) {
                System.out.println("id could not be saved. " + firebaseError.getMessage());
                done.countDown();
            } else {
                done.countDown();
            }
        });
    }

    private static void addingAverageNote(String currentMovieId, String currentMovieAverageNote, CountDownLatch done) {

        rootRef.child("movies").child(currentMovieId).child("average_note").setValue(currentMovieAverageNote, (firebaseError, firebase) -> {
            if (firebaseError != null) {
                System.out.println("average_note could not be saved. " + firebaseError.getMessage());
                done.countDown();
            } else {
                done.countDown();
            }
        });
    }

    private static void addingPosterPath(String currentMovieId, String currentMoviePosterPath, CountDownLatch done) {

        rootRef.child("movies").child(currentMovieId).child("poster_path").setValue(currentMoviePosterPath, (firebaseError, firebase) -> {
            if (firebaseError != null) {
                System.out.println("poster_path could not be saved. " + firebaseError.getMessage());
                done.countDown();
            } else {
                done.countDown();
            }
        });
    }

    private static void addingTitle(String currentMovieId, String currentMovieTitle, CountDownLatch done) {

        rootRef.child("movies").child(currentMovieId).child("title").setValue(currentMovieTitle, (firebaseError, firebase) -> {
            if (firebaseError != null) {
                System.out.println("title could not be saved. " + firebaseError.getMessage());
                done.countDown();
            } else {
                done.countDown();
            }
        });
    }

    private static void addingReleaseDate(String currentMovieId, String currentMovieReleaseDate, CountDownLatch done) {

        rootRef.child("movies").child(currentMovieId).child("release_date").setValue(currentMovieReleaseDate, (firebaseError, firebase) -> {
            if (firebaseError != null) {
                System.out.println("title could not be saved. " + firebaseError.getMessage());
                done.countDown();
            } else {
                done.countDown();
            }
        });
    }

    private static void addUploadDate(String currentMovieId, String currentMovieUploadDate, String currentMovieQuality, CountDownLatch done) {

        rootRef.child("movies").child(currentMovieId).child("qualities").child(currentMovieQuality).child("upload_date").setValue(currentMovieUploadDate, (firebaseError, firebase) -> {
            if (firebaseError != null) {
                System.out.println("upload date couldn't be saved" + firebaseError.getMessage());
                done.countDown();
            } else {
                done.countDown();
            }
        });
    }

    private static void addNotPremiumHost(Host currentHost, String currentMovieId, String currentMovieQuality, CountDownLatch done) {

        rootRef.child("movies").child(currentMovieId).child("qualities").child(currentMovieQuality).child(currentHost.getNameOfHost()).child("not_premium").setValue(currentHost.getNotPremiumList(), (firebaseError, firebase) -> {
            if (firebaseError != null) {
                System.out.println("not_premium links" + currentMovieQuality + "could not be saved. " + firebaseError.getMessage());
                done.countDown();
            } else {
                done.countDown();
            }
        });
    }

    private static void addPremiumHost(Host currentHost, String currentMovieId, String currentMovieQuality, CountDownLatch done) {

        rootRef.child("movies").child(currentMovieId).child("qualities").child(currentMovieQuality).child(currentHost.getNameOfHost()).child("premium").setValue(currentHost.getPremiumList(), (firebaseError, firebase) -> {
            if (firebaseError != null) {
                System.out.println("premium links" + currentMovieQuality + "could not be saved. " + firebaseError.getMessage());
                done.countDown();
            } else {
                done.countDown();
            }
        });
    }

    private static void addKnownHostBoolean(String currentMovieId, String currentMovieQuality, Boolean isThereAKnownHost, CountDownLatch done) {

        rootRef.child("movies").child(currentMovieId).child("qualities").child(currentMovieQuality).child("known_hosts").setValue(isThereAKnownHost, (firebaseError, firebase) -> {
            if (firebaseError != null) {
                System.out.println("premium links" + currentMovieQuality + "could not be saved. " + firebaseError.getMessage());
                done.countDown();
            } else {
                done.countDown();
            }
        });
    }

    public static void addingQualityToDB(String currentMovieQuality, String currentMovieId, String currentMovieUploadDate, String currentMovieUrl, Boolean isThereAKnownHost) {

        CountDownLatch done = new CountDownLatch(1);

        addUploadDate(currentMovieId, currentMovieUploadDate, currentMovieQuality, done);
        addUrl(currentMovieId, currentMovieQuality, currentMovieUrl, done);
        addKnownHostBoolean(currentMovieId, currentMovieQuality, isThereAKnownHost, done);

        try {
            done.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void addingMovieToDB(String currentMovieTitle, String currentMovieId, String currentMovieAverageNote, String currentMoviePosterPath, String currentMovieReleaseDate) {

        CountDownLatch done = new CountDownLatch(1);

        addingId(currentMovieId, done);
        addingTitle(currentMovieId, currentMovieTitle, done);
        addingAverageNote(currentMovieId, currentMovieAverageNote, done);
        addingPosterPath(currentMovieId, currentMoviePosterPath, done);
        addingReleaseDate(currentMovieId, currentMovieReleaseDate, done);

        try {
            done.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void addingHostToDB(String currentMovieId, String currentMovieQuality, Host currentHost) {
        CountDownLatch done = new CountDownLatch(1);

        addNotPremiumHost(currentHost, currentMovieId, currentMovieQuality, done);
        addPremiumHost(currentHost, currentMovieId, currentMovieQuality, done);

        try {
            done.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void authenticateToFireBase() {
        Firebase.AuthResultHandler authResultHandler = new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                // Authenticated successfully with payload authData
                //System.out.println("successfully logged in");
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                // Authenticated failed with error firebaseError
                System.out.println("NOT LOGGED IN" + firebaseError);
            }
        };


        rootRef.authWithCustomToken("4LW8LLam7pydxlGwoIDz0677IsjxGmCId9XMtnSJ", authResultHandler);
    }

    public static boolean isInMovieDataBase(String currentMovieId) {
        dataInMovieDataBase = null;
        // Statting the CountDownLatch in order to wait for the action to be done
        CountDownLatch done = new CountDownLatch(1);
        // Get a reference
        Firebase ref = new Firebase("https://zondedldb.firebaseio.com/movies/" + currentMovieId);
        // Attach a listener to read the data
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                dataInMovieDataBase = snapshot;
                done.countDown();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });

        try {
            done.await();
            if (dataInMovieDataBase.exists()) {
                return true;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isInQualityDataBase(String currentMovieId, String currentMovieQuality) {
        dataInQualityDataBase = null;
        CountDownLatch done = new CountDownLatch(1);
        // Get a reference
        Firebase ref = new Firebase("https://zondedldb.firebaseio.com/movies/" + currentMovieId + "/qualities/" + currentMovieQuality);
        // Attach an listener to read the data
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                dataInQualityDataBase = snapshot;
                done.countDown();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });

        try {
            done.await();
            if (dataInQualityDataBase.exists()) {
                return true;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

}
