package com.zonedldb.Utils;

import com.sun.org.apache.xalan.internal.xsltc.dom.NodeSortRecord;
import com.zonedldb.Movie.Host;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ExtractMovieInformation.
 */
public final class ExtractMovieInformation {

    private static Pattern pattern;
    private static Matcher matcher;
    //
    public boolean isThereAKnownHost = false;
    private boolean isBanned = false;
    private List<Host> hostList = new ArrayList<>();
    private String nameOfHost = "unknown host";
    boolean isInPremiumPart = false;
    boolean isInLinksPart = false;

    Element currentArticleElement;

    // TODO: test and throw something if doesn't find anything...

    public ExtractMovieInformation(Element currentArticleElement) {
        this.currentArticleElement = currentArticleElement;
    }

    public String getTitle() {
        return currentArticleElement.getElementsByClass("titrearticles").select("a[href]").text();
    }

    // TODO: manage when there isn't any quality found... take the little one in the up left corner of the article
    public String getQuality() {

        String qualityToReturn = "no quality found";
        boolean hasMainQuality = currentArticleElement.select("div[id]").select("div[align=center]").select("span[style]").select("span[style=color:#FF0000]").select("b").hasText();
        boolean hasSecondQuality = currentArticleElement.select("span[class=link_cat]").select("a[href]").last().hasText();

        if (hasMainQuality) {
            String mainQuality = currentArticleElement.select("div[id]").select("div[align=center]").select("span[style]").select("span[style=color:#FF0000]").select("b").text().replaceAll("\\]", "").replaceAll("\\[", "").replaceAll("$", "").replaceAll("#", "").replaceAll("\\.", "").toLowerCase();
            //return mainQuality;
            qualityToReturn = taggingQuality(mainQuality);
        } else if (hasSecondQuality) {
            String secondQuality = currentArticleElement.select("span[class=link_cat]").select("a[href]").last().text();
            qualityToReturn = taggingQuality(secondQuality);
        }
        return qualityToReturn;
        // return currentArticleElement.select("span[class=link_cat]").select("a[href]").last().text();
    }

    public String getReleaseYear() {
        if (currentArticleElement.select("b:contains(Date").hasText()) {

            pattern = Pattern.compile("([0-9]{4})");
            matcher = pattern.matcher(currentArticleElement.select("b:contains(Date").first().nextSibling().toString());
            while (matcher.find()) {
                String dateOfRelease = matcher.group();
                TmdbCalls.useYear = true;
                return dateOfRelease;
            }
        }
        return "";
    }

    public String getProductionYear() {
        if (currentArticleElement.select("b:contains(Année").hasText()) {
            TmdbCalls.useYear = true;
            return currentArticleElement.select("b:contains(Année").first().nextSibling().toString();
        }
        return "";
    }

    public String getUrl() {
        return currentArticleElement.getElementsByClass("titrearticles").select("a[href]").attr("href");
    }

    public List<Host> getLinks(String url) {

        hostList.clear();
        nameOfHost = "unknown host";
        isInPremiumPart = false;
        isInLinksPart = false;

        Document movieInformationDocument = null;
        try {
            movieInformationDocument = Jsoup
                    .connect(url)
                    .userAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36")
                    .timeout(5000)
                    .get();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        // Part to try on file in local - Test purpose
//
//        File input = new File("/Users/ghyslainbruno/Downloads/apprenti_sorcier.htm");
//        Document movieInformationDocument = null;
//        try {
//            movieInformationDocument = Jsoup.parse(input, "UTF-8", "http://example.com/");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        // TODO: doing an action when banned from the website
        if (movieInformationDocument.select("div[class=wmsg]").hasText()) {
            isBanned = true;
            // TODO: something here to (try to) change the situation
            // Make the whole thing to wait until a solution is found.
            // If nothing appears then stop it
            System.out.println("Banned from website for now, trying to reboot router, please wait...");
            System.exit(0);
        }

        // TODO: think about handling the following exception not handled

        Elements links = movieInformationDocument.select("div[class=contentl]");

        // TODO: Remove all duplicate code & naming elements with understandable names
        firstForLoop:
        for (Node currentEl : links) {
            for (Node currentEl2 : currentEl.childNodes()) {
                for (Node currentNode : currentEl2.childNodes()) {
                    for (Node currentNode2 : currentNode.childNodes()) {

                        if (currentNode2 instanceof TextNode) {
                            if (((TextNode) currentNode2).text().toLowerCase().contains("nfo")) {
                                break firstForLoop;
                            }
                        }

                        if (currentNode2 instanceof Element) {
                            if (((Element) currentNode2).text().toLowerCase().contains("nfo")) {
                                break firstForLoop;
                            }
                        }

                        if (currentNode2.childNodeSize() <= 1) {

                            selectingHostAndLink(currentNode2);
                            continue;
                        }


                        for (Node currentNode3 : currentNode2.childNodes()) {

                            if (currentNode3 instanceof TextNode) {
                                if (((TextNode) currentNode3).text().toLowerCase().contains("nfo")) {
                                    break firstForLoop;
                                }
                            }

                            if (currentNode3 instanceof Element) {
                                if (((Element) currentNode3).text().toLowerCase().contains("nfo")) {
                                    break firstForLoop;
                                }
                            }


                            selectingHostAndLink(currentNode3);

                        }
                    }
                }
            }
        }

        if (hostList.size() == 0) {
            isThereAKnownHost = false;
        }

        if (isThereAKnownHost) {
            return removeUnknownHosts(hostList);
        }

        return hostList;
    }

    private void selectingHostAndLink(Node currentElementNode) {
        if (currentElementNode instanceof TextNode) {
            ((TextNode) currentElementNode).text(((TextNode) currentElementNode).text().toLowerCase());
            if (((TextNode) currentElementNode).text().contains("premium")) {
                isInPremiumPart = true;
            }
        }

        if (currentElementNode instanceof Element) {

            ((Element) currentElementNode).text(((Element) currentElementNode).text().toLowerCase());

            Element currentElementNode1 = (Element) currentElementNode;

            if (!isInPremiumPart) {
                isInPremiumPart = currentElementNode1.select("span:contains(premium)").hasText();
                if (!isInPremiumPart) {
                    if (currentElementNode1.ownText().toLowerCase().contains("prem")) {
                        isInPremiumPart = true;
                    }
                }
            }

            // This part checks if we've passed the "liens.png" img --> So we can start the scrap
            if (!isInLinksPart) {
                isInLinksPart = currentElementNode1.select("img").attr("src").contains("liens.png");
            } else {

                if (!currentElementNode1.hasAttr("class")) {
                    if (currentElementNode1.hasAttr("style")) {
                        String name = currentElementNode1.text();

                        if (name.contains("uptobox")) {
                            nameOfHost = "uptobox";
                            isThereAKnownHost = true;
                        } else if (name.contains("uplea")) {
                            nameOfHost = "uplea";
                            isThereAKnownHost = true;
                        } else if (name.contains("1fichier")) {
                            nameOfHost = "1fichier";
                            isThereAKnownHost = true;
                        } else if (name.contains("uploaded")) {
                            nameOfHost = "uploaded";
                            isThereAKnownHost = true;
                        } else if (name.contains("rapidgator")) {
                            nameOfHost = "rapidgator";
                            isThereAKnownHost = true;
                        } else if (name.contains("turbobit")) {
                            nameOfHost = "turbobit";
                            isThereAKnownHost = true;
                        } else if (name.contains("free")) {
                            nameOfHost = "free";
                            isThereAKnownHost = true;
                        } else {
                            nameOfHost = name.replaceAll("\\.","").replaceAll("#","").replaceAll("$","").replaceAll("\\[","").replaceAll("\\]","");
                            isThereAKnownHost = true;
                        }
                    }

                    if (currentElementNode1.hasAttr("href")) {

                        Host currentHost = findHost(hostList, nameOfHost);
                        boolean isInHostList = isInHostList(hostList, nameOfHost);

                        String currentLink = currentElementNode1.attr("href");

                        if (!isInPremiumPart) {
                            currentHost.addToFree(currentLink);
                        }
                        if (isInPremiumPart) {
                            currentHost.addToPremium(currentLink);
                        }
                        if (!isInHostList) {
                            hostList.add(currentHost);
                        }
                    }
                }
            }

        }
    }

    public String getUploadDate() {
        final String OLD_FORMAT = "dd-MM-yyyy";
        final String NEW_FORMAT = "yyyy-MM-dd";

        String oldDateString = currentArticleElement.select("time").text().replaceAll("é", "e").replaceAll("û", "u").replace("Janvier", "01").replace("Fevrier", "02").replace("Mars", "03").replace("Avril", "04").replace("Mai", "05").replace("Juin", "06").replace("Juillet", "07").replace("Aout", "08").replace("Septembre", "09").replace("Octobre", "10").replace("Novembre", "11").replace("Decembre", "12").replaceAll(" ", "-");
        String newDateString;

        // Maybe not a good idea using locale.US --> wait & see
        SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT, Locale.US);
        Date d = null;
        try {
            d = sdf.parse(oldDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sdf.applyPattern(NEW_FORMAT);
        newDateString = sdf.format(d);

        return newDateString;

    }

    private static Host findHost(List<Host> hostList, String nameOfCurrentHost) {
        for (Host currentHost : hostList) {
            if (currentHost.getNameOfHost().equals(nameOfCurrentHost)) {
                return currentHost;
            }
        }
        Host newHost = new Host();
        newHost.setNameOfHost(nameOfCurrentHost);
        return newHost;
    }

    private static boolean isInHostList(List<Host> hostList, String nameOfCurrentHost) {
        for (Host currentHost : hostList) {
            if (currentHost.getNameOfHost().equals(nameOfCurrentHost)) {
                return true;
            }
        }
        return false;
    }

    private static String taggingQuality(String qualityGiven) {

        String qualityToReturn = "";

        if (qualityGiven.contains("1080p")) {
            qualityToReturn = qualityToReturn + " 1080p ";
        }
        if (qualityGiven.contains("720p")) {
            qualityToReturn = qualityToReturn + " 720p ";
        }
        if (qualityGiven.contains("multilangues")) {
            qualityToReturn = qualityToReturn + " multilangues ";
        }
        if (qualityGiven.contains("dts")) {
            qualityToReturn = qualityToReturn + " dts ";
        }
        if (qualityGiven.contains("truefrench")) {
            qualityToReturn = qualityToReturn + " truefrench ";
        }
        if (qualityGiven.contains("x264")) {
            qualityToReturn = qualityToReturn + " h264 ";
        }
        if (qualityGiven.contains("x265")) {
            qualityToReturn = qualityToReturn + " h265 ";
        }
        if (qualityGiven.contains("h264")) {
            qualityToReturn = qualityToReturn + " h264 ";
        }
        if (qualityGiven.contains("h265")) {
            qualityToReturn = qualityToReturn + " h265 ";
        }

        return qualityToReturn.trim().replaceAll("  ", " | ");
    }

    // Method not tested yet when having knonwn host AND unknown hosts
    public List<Host> removeUnknownHosts(List<Host> hostList) {
        int unknownHostsIndex = 0;
        boolean isThereUnknownHosts = false;

        for (Host currentHost : hostList) {
            if (currentHost.getNameOfHost().equals("unknown host")) {
                unknownHostsIndex = hostList.indexOf(currentHost);
                isThereUnknownHosts = true;
            }
        }
        if (isThereUnknownHosts) {
            hostList.remove(unknownHostsIndex);
            return hostList;
        }
        return hostList;
    }

}