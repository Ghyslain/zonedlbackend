package com.zonedldb.Utils;

/**
 * Created by ghyslain on 20/07/2016.
 */
public class IsInMovieList {
    private int id;
    private boolean isInMovieListBool;

    // TODO: IsInMovieList.java class never used anymore

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isInMovieListBool() {
        return isInMovieListBool;
    }

    public void setIsInMovieListBool(boolean isInMovieListBool) {
        this.isInMovieListBool = isInMovieListBool;
    }
}
