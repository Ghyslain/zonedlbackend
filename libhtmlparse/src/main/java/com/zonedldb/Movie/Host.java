package com.zonedldb.Movie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ghyslain on 09/08/2016.
 */
public class Host {

    private String nameOfHost;
    public List<String> notPremiumList = new ArrayList<String>();
    public List<String> premiumList = new ArrayList<String>();
    private Map<String, List<String>> linksMap = new HashMap<String, List<String>>();

    public List<String> getPremiumList() {
        return premiumList;
    }

    public List<String> getNotPremiumList() {
        return notPremiumList;
    }

    public String getNameOfHost() {
        return nameOfHost;
    }

    public void setNameOfHost(String nameOfHost) {
        this.nameOfHost = nameOfHost;
    }

    public void addToPremium(String link) {
        premiumList.add(link);
        linksMap.put("premium", premiumList);
    }

    public void addToFree(String link) {
        notPremiumList.add(link);
        linksMap.put("free", notPremiumList);
    }
}
