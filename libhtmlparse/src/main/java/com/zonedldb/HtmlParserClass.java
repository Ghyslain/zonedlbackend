package com.zonedldb;

import com.zonedldb.DataBase.FirebaseCalls;
import com.zonedldb.Movie.Host;
import com.zonedldb.Utils.ExtractMovieInformation;
import com.zonedldb.Utils.TmdbCalls;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

public class HtmlParserClass {

    public static void main(String[] args) {

        // testing writing console outputs into a log
        try {
            FileOutputStream f = new FileOutputStream("log.txt");
            System.setOut(new PrintStream(f));
            System.setErr(new PrintStream(f));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String moviesListUrl = "https://www.zone-telechargement.com/films-bluray-hd.html?&displaychangeto=large";

        TmdbCalls tmdbInformation;

        // TODO: using the update date and rescrap article if updated the same day

        FirebaseCalls.authenticateToFireBase();

        try {
            Document moviesListDocument = Jsoup
                    .connect(moviesListUrl)
                    .userAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36")
                    .timeout(5000)
                    .get();

            String lastPageNumberString = moviesListDocument.getElementsByClass("navigation2").select("a[href]").select("a:contains(Suivant)").first().previousElementSibling().text();
            int lastPageNumber = Integer.parseInt(lastPageNumberString);

            for (int i = lastPageNumber; i > 1; i--) {

                moviesListDocument = Jsoup
                        .connect("https://www.zone-telechargement.com/films-bluray-hd.html?&displaychangeto=large&page=" + i)
                        .userAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36")
                        .timeout(5000)
                        .get();

                Elements articles = moviesListDocument.getElementsByClass("base");

                for (Element currentArticle : articles) {

                    ExtractMovieInformation movieInformationExtractor = new ExtractMovieInformation(currentArticle);
                    String currentMovieTitle = movieInformationExtractor.getTitle();
                    String currentMovieQuality = movieInformationExtractor.getQuality();
                    String currentMovieUrl = movieInformationExtractor.getUrl();
                    String currentMovieUploadDate = movieInformationExtractor.getUploadDate();
                    String currentMovieReleaseYear = movieInformationExtractor.getReleaseYear();
                    String currentMovieProductionYear = movieInformationExtractor.getProductionYear();

                    tmdbInformation = new TmdbCalls(currentMovieTitle, currentMovieReleaseYear);
                    tmdbInformation.load();

                    // If no result from TMDB --> use the production Year
                    if (!tmdbInformation.isThereAResult()) {
                        tmdbInformation = new TmdbCalls(currentMovieTitle, currentMovieProductionYear);
                        tmdbInformation.load();
                    }

                    // If still no result from TMDB --> don't use any year
                    if (!tmdbInformation.isThereAResult()) {
                        TmdbCalls.useYear = false;
                        tmdbInformation.load();
                    }

                    // If still no result from TMDB --> currentMovie not added to DB
                    if (!tmdbInformation.isThereAResult()) {
                        System.out.println("no tmdb result - movie skipped");
                        continue;
                    }


                    // TODO: handling when having no results from tmdb, check for total_results number
                    String currentMovieId = tmdbInformation.getId();
                    String currentMovieAverageNote = tmdbInformation.getAverageNote();
                    String currentMoviePosterPath = tmdbInformation.getPosterPath();
                    String currentMovieReleaseDate = tmdbInformation.getReleaseDate();
                    currentMovieTitle = tmdbInformation.getTitle();

                    // Using this snippet to break for a particular movie

                    if (currentMovieId.equals("36648")) {
                        // breaking point here
                        System.out.println("stopping here");
                    }

                    System.out.println(currentMovieTitle + " - id: " + currentMovieId);

                    boolean isInMovieDataBaseBool = FirebaseCalls.isInMovieDataBase(currentMovieId);

                    if (isInMovieDataBaseBool) {
                        boolean isInQualityBaseBool = FirebaseCalls.isInQualityDataBase(currentMovieId, currentMovieQuality);
                        if (isInQualityBaseBool) {
                            System.out.println("movie and quality already in db - nothing to do");
                        } else {
                            System.out.println("movie in the list but quality not - adding quality");

                            List<Host> hostsAndLinksList = movieInformationExtractor.getLinks(currentMovieUrl);

                            if (!movieInformationExtractor.isThereAKnownHost) {
                                FirebaseCalls.addingQualityToDB(currentMovieQuality, currentMovieId, currentMovieUploadDate ,currentMovieUrl, movieInformationExtractor.isThereAKnownHost);
                                System.out.println("no known host found - movie added for next time scrap purpose");
                                continue;
                            }

                            FirebaseCalls.addingQualityToDB(currentMovieQuality, currentMovieId, currentMovieUploadDate ,currentMovieUrl, movieInformationExtractor.isThereAKnownHost);

                            for (Host currentHost : hostsAndLinksList) {
                                FirebaseCalls.addingHostToDB(currentMovieId, currentMovieQuality, currentHost);
                            }
                            System.out.println("movie done");
                        }
                    } else {
                        System.out.println("movie not in movie list - adding movie");
                        FirebaseCalls.addingMovieToDB(currentMovieTitle, currentMovieId, currentMovieAverageNote, currentMoviePosterPath, currentMovieReleaseDate);
                        List<Host> hostsAndLinksList = movieInformationExtractor.getLinks(currentMovieUrl);
                        if (!movieInformationExtractor.isThereAKnownHost) {
                            FirebaseCalls.addingQualityToDB(currentMovieQuality, currentMovieId, currentMovieUploadDate, currentMovieUrl, movieInformationExtractor.isThereAKnownHost);
                            System.out.println("no known host found - movie added for next time scrap purpose");
                            continue;
                        }

                        FirebaseCalls.addingQualityToDB(currentMovieQuality, currentMovieId, currentMovieUploadDate, currentMovieUrl, movieInformationExtractor.isThereAKnownHost);
                        for (Host currentHost : hostsAndLinksList) {
                            FirebaseCalls.addingHostToDB(currentMovieId, currentMovieQuality, currentHost);
                        }
                        System.out.println("movie done");
                    }
                }
                System.out.println("Page " + i + " done");
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

